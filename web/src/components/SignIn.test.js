import React from "react";
import wait from "waait";
import { mount } from "enzyme";
import toJSON, { mountToJson } from "enzyme-to-json";
import { MockedProvider } from "react-apollo/test-utils";

import SignIn, { MUTATION_SIGN_IN } from "./SignIn";

const mocksBeforeSignIn = [
  {
    request: { query: MUTATION_SIGN_IN },
    result: { data: null }
  }
];

describe("SignIn Component", () => {
  test("Mount Snapshot", async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocksBeforeSignIn}>
        <SignIn />
      </MockedProvider>
    );

    await wait();
    wrapper.update();

    expect(toJSON(wrapper.find("form"))).toMatchSnapshot();
  });

  test("Form Displayed", async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocksBeforeSignIn}>
        <SignIn />
      </MockedProvider>
    );

    const WrapperForm = wrapper.find("form");
    expect(WrapperForm.exists()).toBe(true);
  });
});
