import React from "react";
import wait from "waait";
import { mount } from "enzyme";
import toJSON, { mountToJson } from "enzyme-to-json";
import { MockedProvider } from "react-apollo/test-utils";

import SignUp, { MUTATION_SIGN_UP } from "./SignUp";

const mocksBeforeSignIn = [
  {
    request: { query: MUTATION_SIGN_UP },
    result: { data: null }
  }
];

describe("SignUp Component", () => {
  test("Mount Snapshot", async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocksBeforeSignIn}>
        <SignUp />
      </MockedProvider>
    );
    await wait();
    wrapper.update();

    expect(toJSON(wrapper.find("form"))).toMatchSnapshot();
  });
});
