import React, { Component } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

import Error from "./Error";

export const QUERY_USER_GET = gql`
  query QUERY_USER_GET {
    user {
      code
      error
      message
      user {
        id
        name
        email
        token
      }
    }
  }
`;

class User extends Component {
  render() {
    return (
      <Query query={QUERY_USER_GET}>
        {({ error, loading, data }) => {
          if (error) return <Error />;

          if (loading) return <div>Loading...</div>;

          const { code, error: errMsg, message, user } = data.user;

          if (code === 401) {
            return <div>Please SignIn Again.</div>;
          }

          if (code !== 200) {
            return <div>{errMsg}</div>;
          }

          return (
            <div>
              <h1>Name: {user.name}</h1>
              <h3>Email: {user.email}</h3>
              <p>Id: {user.id}</p>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default User;
