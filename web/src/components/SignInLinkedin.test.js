import React from "react";
import wait from "waait";
import { mount } from "enzyme";
import toJSON, { mountToJson } from "enzyme-to-json";
import { MockedProvider } from "react-apollo/test-utils";

import SignInLinkedin from "./SignInLinkedin";

describe("SignIn Component", () => {
  test("Mount Snapshot", async () => {
    const wrapper = mount(<SignInLinkedin />);

    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  test("Button Clicked", async () => {
    const wrapper = mount(<SignInLinkedin />);

    //NOTE: window.location.href having some issue in jsdom, this is a work around
    delete global.window.location;
    global.window.location = { href: "" };

    wrapper.find("button").simulate("click");
  });
});
