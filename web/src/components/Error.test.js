import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";

import Error from "./Error";

describe("Error Component", () => {
  test("Render", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Error />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test("snapshot test", () => {
    const wrapper = shallow(<Error />);

    expect(toJSON(wrapper)).toMatchSnapshot()
  });
});
