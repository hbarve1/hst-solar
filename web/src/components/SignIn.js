import React, { Component } from "react";
import styled from "@emotion/styled";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

import Error from "./Error";

import { QUERY_USER_GET } from "./User";

export const MUTATION_SIGN_IN = gql`
  mutation MUTATION_SIGN_IN($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      code
      error
      message
      user {
        id
        name
        email
      }
    }
  }
`;

export const Form = styled.form`
  width: 40%;
  /* box-shadow: 4px 4px 12px -3px black; */
  border-radius: 4px;

  fieldset {
    display: flex;
    flex-direction: column;
    border-radius: 4px;

    div {
      text-align: end;

      button {
        width: 100px;
        height: 40px;
        border-radius: 4px;
        box-shadow: 4px 4px 12px -3px black;
        font-size: 1.5em;

        margin: 10px;
      }
    }
  }
`;

export const Label = styled.label`
  padding: 16px;
  margin: 10px;
  border-radius: 4px;
  box-shadow: 4px 4px 12px -3px black;
  height: 80px;
  font-size: 1.5em;

  input {
    float: right;
    border-radius: 4px;
    height: 37px;
    width: 273px;

    &::placeholder {
      font-size: 1em;
      padding-left: 12px;
    }
  }
`;

class SignIn extends Component {
  state = {
    email: "",
    password: ""
  };

  handleChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    return (
      <Mutation
        mutation={MUTATION_SIGN_IN}
        variables={this.state}
        refetchQueries={[
          {
            query: QUERY_USER_GET
          }
        ]}
      >
        {(signIn, { error, loading, data }) => {
          if (error) {
            // console.log(error);
            return <Error error={error} />;
          }

          if (loading) return <div>loading...</div>;

          return (
            <Form
              onSubmit={async e => {
                e.preventDefault();
                const { email, password } = this.state;
                await signIn();

                this.setState({ email: "", password: "" });
              }}
            >
              <fieldset>
                <Label htmlFor="email">
                  Email:
                  <input
                    type="email"
                    name="email"
                    placeholder="Name"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                </Label>

                <Label htmlFor="password">
                  Password:
                  <input
                    type="password"
                    name="password"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                </Label>

                <div>
                  <button type="submit">Submit</button>
                </div>
              </fieldset>
            </Form>
          );
        }}
      </Mutation>
    );
  }
}

export default SignIn;
