import React, { Component } from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

import Error from "./Error";

import { Form, Label } from "./SignIn";

export const MUTATION_SIGN_UP = gql`
  mutation SignUp($name: String!, $email: String!, $password: String!) {
    signUp(name: $name, email: $email, password: $password) {
      code
      error
      message
      user {
        id
        name
        email
      }
    }
  }
`;

class SignUp extends Component {
  state = {
    name: "",
    email: "",
    password: ""
  };

  handleChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    return (
      <Mutation mutation={MUTATION_SIGN_UP} variables={this.state}>
        {(signUp, { error, loading, data }) => {
          // console.log(error);

          if (error) return <Error error={error} />;

          if (loading) return <div>Loading...</div>;

          // console.log(data);

          return (
            <Form
              onSubmit={async e => {
                e.preventDefault();
                await signUp();
                this.setState({ name: "", email: "", password: "" });
              }}
            >
              <fieldset>
                <Label htmlFor="name">
                  Name:
                  <input
                    type="name"
                    name="name"
                    placeholder="Name"
                    value={this.state.name}
                    onChange={this.handleChange}
                  />
                </Label>

                <Label htmlFor="email">
                  Email:
                  <input
                    type="email"
                    name="email"
                    placeholder="Name"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                </Label>

                <Label htmlFor="password">
                  Password:
                  <input
                    type="password"
                    name="password"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                </Label>

                <div>
                  <button type="submit">Submit</button>
                </div>
              </fieldset>
            </Form>
          );
        }}
      </Mutation>
    );
  }
}

export default SignUp;
