import React, { Component } from "react";
import styled from "@emotion/styled";

const Button = styled.button`
  width: 220px;
  height: 70px;
  font-weight: 500;
  font-size: 1.5rem;
  box-shadow: 4px 4px 12px 0px black;
  border-radius: 4px;
`;

class SignInLinkedin extends Component {
  render() {
    return (
      <Button
        type="button"
        onClick={() => {
          window.location.href = `${
            process.env.REACT_APP_SERVER_URI
          }/auth/linkedin`;
        }}
      >
        SignIn with Linkedin
      </Button>
    );
  }
}

export default SignInLinkedin;
