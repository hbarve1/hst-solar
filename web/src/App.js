import React, { Component } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import styled from "@emotion/styled";

import logo from "./logo.svg";
import "./App.css";

import SignIn from "./components/SignIn";
import SignUp from "./components/SignUp";
import User from "./components/User";
import SignInLinkedin from "./components/SignInLinkedin";

const client = new ApolloClient({
  uri: process.env.REACT_APP_APOLLO_CLIENT_URI,
  credentials: "include"
});

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.header`
  display: flex;
  align-items: center;
`;

const LogoContainer = styled.div`
  width: 100px;
  height: 100px;

  img {
    width: 100%;
    height: 100%;
  }
`;

const Heading = styled.h1`
  text-align: center;
  flex-grow: 1;
`;

const BodyContainer = styled.div`
  margin: 10px;
  display: flex;
  justify-content: space-around;
`;

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Container>
          <Header>
            <LogoContainer>
              <img src={logo} alt="logo" />
            </LogoContainer>

            <Heading>HST Solar</Heading>
          </Header>

          <BodyContainer>
            <User />
          </BodyContainer>

          <BodyContainer>
            <SignInLinkedin />
          </BodyContainer>

          <BodyContainer>
            <SignIn />
            <SignUp />
          </BodyContainer>
        </Container>
      </ApolloProvider>
    );
  }
}

export default App;
