# HST Solar

HST Solar assignment

## Web

This folder contains the code for front-end. It need a ".env" file

## Server

This folder contains the code for Server in Node.js

## MongoDB

configuration for mongodb database is done in 'docker-compose.yml' file.

This command will start mongodb server.

    $docker-compose up

