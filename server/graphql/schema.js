const { gql } = require("apollo-server");

const typeDefs = gql`
  interface MutationResponse {
    code: Int!
    error: String
    message: String
  }

  type User {
    id: ID!
    email: String!
    name: String!
    password: String
    token: String
  }

  type UserQueryResponse implements MutationResponse {
    code: Int!
    error: String
    message: String
    user: User
  }

  type SignUpMutationResponse implements MutationResponse {
    code: Int!
    error: String
    message: String
    user: User
  }

  type SignInMutationResponse implements MutationResponse {
    code: Int!
    error: String
    message: String
    user: User
  }

  type Query {
    user: UserQueryResponse!
  }

  type Mutation {
    signIn(email: String!, password: String!): SignInMutationResponse!
    signUp(
      email: String!
      name: String!
      password: String!
    ): SignUpMutationResponse!
  }
`;

module.exports = typeDefs;
