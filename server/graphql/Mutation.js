const util = require("util");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const db = require("../database");

const jwtSignPromise = util.promisify(jwt.sign);
const jwtVerifyPromise = util.promisify(jwt.verify);

const Mutation = {
  async signUp(parent, args, ctx, info) {
    const { name, email, password } = args;

    // TODO: encrypt password
    return await db.createUser({ name, email, password });
  },
  async signIn(parent, args, ctx, info) {
    const { email, password } = args;

    // console.log(ctx.req.res);

    // TODO: encrypt password match and generate JWT token
    const res = await db.getUser({
      email
    });

    // console.log(res);

    if (res.code !== 200) {
      return res;
    }

    const token = await jwtSignPromise(
      { userId: res.user.id },
      process.env.TOKEN_SECRET
    );

    // console.log(token);

    ctx.res.cookie("token", token, {
      // httpOnly: true,
      maxAge: 1000 * 60 * 5
    });

    return res;
  }
};

module.exports = Mutation;
