const db = require("../database");

const Query = {
  async user(parent, args, ctx, info) {
    const { token } = ctx.req.cookies;

    if (!token) {
      return {
        code: 401,
        error: "Unauthorized"
      };
    }

    const { userId } = ctx.req;

    let user = await db.UserModal.findById(userId).exec();

    user = JSON.parse(JSON.stringify(user));

    if (!user) {
      return {
        code: 404,
        error: "User Not Found"
      };
    }

    user.id = user._id;
    delete user._id;
    delete user.password;

    return {
      code: 200,
      message: "Success",
      user
    };
  }
};

module.exports = Query;
