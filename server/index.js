// include environment variables
require("dotenv").config({ path: "variables.env" });

const express = require("express");

const { ApolloServer } = require("apollo-server-express");
const passport = require("passport");
const LinkedInStrategy = require("passport-linkedin").Strategy;
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const util = require("util");

const typeDefs = require("./graphql/schema");
const resolvers = require("./graphql/Resolver");
const db = require("./database");

const jwtSignPromise = util.promisify(jwt.sign);
const jwtVerifyPromise = util.promisify(jwt.verify);

const app = express();
const HOST = process.env.HOST || "0.0.0.0";
const PORT = process.env.PORT || 8080;
const URI = `http://${HOST}:${PORT}`;

// configure express
app.use(morgan("dev"));
app.use(cookieParser());
app.use(
  session({
    secret: process.env.TOKEN_SECRET,
    resave: false,
    saveUninitialized: true
  })
);

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((obj, done) => done(null, obj));

passport.use(
  new LinkedInStrategy(
    {
      consumerKey: process.env.LINKEDIN_API_KEY,
      consumerSecret: process.env.LINKEDIN_SECRET_KEY,
      callbackURL: `${URI}/auth/linkedin/callback`
    },
    async function(token, tokenSecret, profile, done) {
      const { id, displayName: name } = profile;

      // making the fake email id for linkedin.
      const email = `${id}-mail@linkedin.com`;

      let isUser = await db.UserModal.findOne({ email }, "name email").exec();

      if (isUser) {
        isUser = JSON.parse(JSON.stringify(isUser));
        isUser.id = isUser._id;
        delete isUser._id;

        return done(null, isUser);
      }

      const newUser = new db.UserModal({
        name,
        email,
        password: email
      });

      const res = await newUser.save();

      return done(null, res);
    }
  )
);

app.use((req, res, next) => {
  const { token } = req.cookies;

  if (token) {
    const { userId } = jwt.verify(token, process.env.TOKEN_SECRET);
    req.userId = userId;
  }

  next();
});

app.get("/", (req, res) => res.send("Hello World!"));

// The request will be redirected to LinkedIn for authentication, so this
// function will not be called.
app.get("/auth/linkedin", passport.authenticate("linkedin"), (req, res) => {});

app.get(
  "/auth/linkedin/callback",
  passport.authenticate("linkedin", { failureRedirect: "/login" }),
  async (req, res) => {
    const token = await jwtSignPromise(
      { userId: req.user.id },
      process.env.TOKEN_SECRET
    );

    res.cookie("token", token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 5
    });

    res.redirect(process.env.WEB_URI);
  }
);

app.get("/logout", function(req, res) {
  req.logout();
  res.redirect("/");
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: req => ({ ...req }),
  // tracing: true,
  resolverValidationOptions: {
    requireResolversForResolveType: false
  }
});

server.applyMiddleware({
  app,
  cors: {
    credentials: true,
    origin: "http://localhost:3000"
  }
});

app.listen(PORT, () => {
  console.log(`🚀 Server ready at ${URI}${server.graphqlPath}`);
});
