const mongoose = require("mongoose");

const DB_HOST = process.env.DB_HOST || "0.0.0.0";
const DB_PORT = process.env.DB_PORT || 27017;
const DB_NAME = process.env.DB_NAME || "test";

const URI = `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`;

mongoose.connect(
  URI,
  { useNewUrlParser: true }
);

const UserModal = mongoose.model("User", {
  name: String,
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true
  },
  password: String
});

async function createUser({ name, email, password }) {
  try {
    const isUser = await UserModal.findOne({ email });

    if (isUser) {
      return {
        code: 400,
        error: "User already exists"
      };
    }

    const newUser = new UserModal({
      name,
      email,
      password
    });

    const user = await newUser.save().exec();

    user.id = user._id.toString();

    return {
      code: 201,
      message: "User account created successfully.",
      user
    };
  } catch (error) {
    return {
      code: 500,
      error: "Server Error"
    };
  }
}

async function getUser({ email }) {
  try {
    const user = await UserModal.findOne({ email });

    if (!user) {
      return {
        code: 404,
        error: "User Not Found"
      };
    }

    return {
      code: 200,
      message: "Success",
      user
    };
  } catch (error) {
    // console.error(error);

    return {
      code: 500,
      error: "Server Error"
    };
  }
}

module.exports = {
  UserModal,
  createUser,
  getUser
};
